-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 25, 2020 alle 18:40
-- Versione del server: 10.1.38-MariaDB
-- Versione PHP: 7.1.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dumpster_service`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `service_variable`
--

CREATE TABLE `service_variable` (
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `value` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `service_variable`
--

INSERT INTO `service_variable` (`name`, `value`) VALUES
('isAvailable', 1),
('isForced', 1),
('isTokenAssigned', 0),
('timerEnd', 0),
('wMax', 100);

-- --------------------------------------------------------

--
-- Struttura della tabella `weight_history`
--

CREATE TABLE `weight_history` (
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `weight` int(10) NOT NULL,
  `n_deposits` int(10) NOT NULL,
  `weight_variation` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dump dei dati per la tabella `weight_history`
--

INSERT INTO `weight_history` (`date`, `weight`, `n_deposits`, `weight_variation`) VALUES
('2020-02-23 16:08:02', 0, 0, 0),
('2020-02-23 16:09:44', 5, 1, 5),
('2020-02-23 16:14:24', 6, 2, 1),
('2020-02-23 16:15:15', 4, 2, -2),
('2020-02-25 16:16:57', 7, 3, 3),
('2020-02-25 18:15:28', 8, 4, 1);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `service_variable`
--
ALTER TABLE `service_variable`
  ADD PRIMARY KEY (`name`);

--
-- Indici per le tabelle `weight_history`
--
ALTER TABLE `weight_history`
  ADD PRIMARY KEY (`date`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
