google.charts.load('current', {'packages':['bar']});
const serverName = 'http://225a5c1b.ngrok.io/smart_dumpster/dumpster_service/';

var days = 0;
var oldDateArray = null;
var forced = 0;
var drawed = false;

$(document).ready(function () {
    //code before the pause
    setTimeout(function(){
        //do what you need here
        updatePage();
        setInterval(function(){
            updatePage();
        }, 500);
        $("#forcedArea").click(function (e) {
            newForced = forced + 1 - 2 * forced;
            const params = "state=" + newForced;
            fun = function(xhttp) {
                console.log(xhttp);
                if(xhttp === "error"){
                    return false;
                }
                return true;
            }
            if(loadDocPostSync(serverName + "setForcedDashboard.php", fun, params)){
                forced = newForced;
            }
            console.log(forced);
            
            updateForcedState();
        });
    }, 1000);
});

function navBtnPress(){
    $("#stateBtn").attr('disabled', function(index, attr){return !attr;});
    $("#statsBtn").attr('disabled', function(index, attr){return !attr;});
    $("#stateBtn").toggleClass("disabled");
    $("#statsBtn").toggleClass("disabled");
    $("#stateBtn").toggleClass("active");
    $("#statsBtn").toggleClass("active");
    $("#stateArea").toggleClass("hidden");
    $("#statisticsArea").toggleClass("hidden");
}

function updatePage(){
    const params = "days=" + days;
    fun = function(xhttp) {
        console.log(xhttp);
        if(xhttp === "error"){
            return false;
        }
        info = JSON.parse(xhttp);
        updateTable(info);
        drawChart(info);
        forced = info["isForced"];
        if(!drawed){
            drawed = true;
            updateForcedState()
        }
        return true
    }
    loadDocPostAsync(serverName + "getInfoDashboard.php", fun, params);
}

function updateForcedState(){
    if(forced == 1){
        $("#forcedArea").html(`<em class="fas fa-check-circle fa-9x col-12"></em>
        <p>Return Available</p>`);
    } else {
        $("#forcedArea").html(`<em class="fas fa-times-circle fa-9x col-12"></em>
        <p>Force Not Available</p>`);
    }
}

function updateTable(info){
    $("#avSpace").text(parseInt(info["wMax"], 10) - parseInt(info["actual"]["weight"], 10));
    $("#actSpace").text(info["actual"]["weight"]);
    $("#nDeposits").text(info["actual"]["n_deposits"]);
    $("#isAvaileble").text(info["isAvailable"]);
}

function enterDays(){
    days = $("#nDays").val();
}

function drawChart(info) {
    
    let dateArray = [['Day', 'N.Deposits', 'Quantity(Kg)']];
    dateArray = dateArray.concat(info["history"]);
    
    if(oldDateArray != null){
        let different = false;
        if(oldDateArray.length != dateArray.length) different = true;
        else{
            for (let i = 0; i < oldDateArray.length; i++) {
                for (let j = 0; j < oldDateArray[i].length; j++) {
                    if(oldDateArray[i][j] != dateArray[i][j]){
                        different = true;
                        break;
                    } 
                }
            }
        }
        if(!different) return;
    }
    oldDateArray = dateArray;
    
    let data = google.visualization.arrayToDataTable(dateArray);
    
    let options = {
        width: 900,
        chart: {
            title: 'Dumpster Trend',
            subtitle: 'number of deposits, quantities deposited'
        },
        bars: 'vertical'
    };
    
    let chart = new google.charts.Bar(document.getElementById('columnchart_material'));
    
    chart.draw(data, google.charts.Bar.convertOptions(options));
}

