<?php
require_once("bootstrap.php");
$token = $_GET["token"];
if($dbh->isTokenAssignable()){
    $dbh->assignToken();
    $decryptedToken = decrypt($token);
    echo "token:".$decryptedToken;
} else {
    echo "auth:error";
}

function decrypt($var){
    $encryptionKey = 8;
    $res = $var;
    for($i = 0; $i < strlen($var); $i++){
        $res[$i] = chr(ord($var[$i]) - $encryptionKey);
    }
    return $res;
}
?>