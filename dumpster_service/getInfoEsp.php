<?php
    require_once("bootstrap.php");
    $stateInfo = $dbh->getStateInfo();
    echo json_encode(array("wMax" => $stateInfo["wMax"], "isForced" => $stateInfo["isForced"]));
?>
