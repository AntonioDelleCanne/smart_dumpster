<?php
    require_once("bootstrap.php");
    $days = $_POST["days"];
    $stateInfo = $dbh->getStateInfo();
    $history = $dbh->getWeightHistory($days);
    $actualInfo = $dbh->getActualState();
    echo json_encode(array("actual" => $actualInfo, "history" => $history, "wMax" => $stateInfo["wMax"], "isAvailable" => $stateInfo["isAvailable"], "isForced" => $stateInfo["isForced"], "isTokenAssigned" => $stateInfo["isTokenAssigned"]));
?>
