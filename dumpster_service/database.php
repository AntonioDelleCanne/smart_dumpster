<?php

class DatabaseHelper{
    private $db;
    
    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if($this->db->connect_error){
            die("Connesione fallita al db");
        }
    }

    public function setForcedState($newState){
        $stmt = $this->db->prepare("UPDATE service_variable SET value = ? WHERE name = 'isForced'");
        $stmt->bind_param("i", $newState);
        $stmt->execute();
    }
    
    public function isAvailableUpdate($isAvailable){
        $stmt = $this->db->prepare("UPDATE service_variable SET value = ? WHERE name = 'isAvailable'");
        $stmt->bind_param("i", $isAvailable);
        $stmt->execute();
    }

    public function freeToken(){
        $isTokenAssigned = 0;
        $stmt = $this->db->prepare("UPDATE service_variable SET value = ? WHERE name = 'isTokenAssigned'");
        $stmt->bind_param("i", $isTokenAssigned);
        $stmt->execute();
    }
    
    public function assignToken(){
        $isTokenAssigned = 1;
        $stmt = $this->db->prepare("UPDATE service_variable SET value = ? WHERE name = 'isTokenAssigned'");
        $stmt->bind_param("i", $isTokenAssigned);
        $stmt->execute();
        $timerEnd = time() + 3;
        $stmt = $this->db->prepare("UPDATE service_variable SET value = ? WHERE name = 'timerEnd'");
        $stmt->bind_param("i", $timerEnd);
        $stmt->execute();
    }

    private function getServiceVariableValue($serviceVariable){
        $stmt = $this->db->prepare("SELECT value FROM service_variable WHERE name = ?");
        $stmt->bind_param("s", $serviceVariable);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_array()[0];
    }

    public function isTokenAssignable(){
        $timerEnd = $this->getServiceVariableValue('timerEnd');
        $isTokenAssigned = $this->getServiceVariableValue('istokenAssigned');
        $isForced = $this->getServiceVariableValue('isForced');
        $isAvailable = $this->getServiceVariableValue('isAvailable');
        return !$isForced && $isAvailable && !($isTokenAssigned && $timerEnd > time());
    }
    
    public function getStateInfo(){
        $stmt = $this->db->prepare("SELECT * FROM service_variable");
        $stmt->execute();
        $result = $stmt->get_result();
        $res = array();
        foreach ($result->fetch_all(MYSQLI_ASSOC) as $key => $value) {
            $res[$value["name"]] = $value["value"];
        }
        return $res;
    }
    
    public function getWeightHistory($nDays){
        $stmt = $this->db->prepare('SELECT DATE_FORMAT(date, "%d/%m/%Y"), CAST(COUNT(*) as CHAR), SUM(weight_variation)  FROM weight_history WHERE weight_variation > 0 AND DATEDIFF(NOW(), date) <= ? GROUP BY YEAR(date),MONTH(date),DAY(date) ORDER BY date ASC');
        $stmt->bind_param("i", $nDays);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all();
    }

    public function getActualState(){
        $stmt = $this->db->prepare('SELECT * FROM weight_history ORDER BY date DESC LIMIT 1');
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_array();
    }

    public function weightUpdate($newWeight){
        $lastVariation = $this->getLastWeightVariation();
        if($lastVariation["weight"] == $newWeight){
            return false;
        }
        #insert new row
        $weightVariation = $newWeight - $lastVariation["weight"];
        $nDesposits = $lastVariation["n_deposits"];
        if($weightVariation > 0){
            $nDesposits++;
        }
        $stmt = $this->db->prepare("INSERT INTO weight_history (weight, n_deposits, weight_variation) VALUES (?, ?, ?)");
        $stmt->bind_param("iii", $newWeight, $nDesposits, $weightVariation);
        $stmt->execute();
    }
    
    public function getLastWeightVariation(){
        $stmt = $this->db->prepare("SELECT * FROM weight_history ORDER BY date DESC");
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_array(MYSQLI_ASSOC);
    }
    
    public function addTime($time){
        $newTime = $this->getServiceVariableValue("timerEnd") + $time;
        $stmt = $this->db->prepare("UPDATE service_variable SET value = ? WHERE name = 'timerEnd'");
        $stmt->bind_param("i", $newTime);
        $stmt->execute();
    }

    public function resetToken(){
        $stmt = $this->db->prepare("UPDATE service_variable SET value = 0 WHERE name = 'isTokenAssigned'");
        $stmt->execute();
    }
    
}
?>